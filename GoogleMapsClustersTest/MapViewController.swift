//
//  MapViewController.swift
//  GoogleMapsClustersTest
//
//  Created by ****** ****** on 02.11.2020.
//

import GoogleMaps
import UIKit


class MapViewController: UIViewController {

    var googleMapsView: GMSMapView?
    var clusterManager: GMUClusterManager?
    let locationsArray = [CLLocationCoordinate2D(latitude: 50.455639, longitude: 30.521833),
                       CLLocationCoordinate2D(latitude: 50.452778, longitude: 30.514444),
                       CLLocationCoordinate2D(latitude: 50.463056, longitude: 30.516389),
                       CLLocationCoordinate2D(latitude: 50.401667, longitude: 30.513611),
                       CLLocationCoordinate2D(latitude: 50.467611, longitude: 30.491944)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMapView()
        setupClusters()
        for location in locationsArray {
            createClusters(location: location)
        }
    }
    
    func setupMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: 50.4566, longitude: 30.5238, zoom: 6.0)
        googleMapsView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = googleMapsView
    }
    
    func placeMarker(location: CLLocationCoordinate2D) {
        let marker = GMSMarker(position: CLLocationCoordinate2DMake(location.latitude, location.longitude))
        let icon = UIImage(named: "MapPoint")?.withRenderingMode(.alwaysTemplate)
        let markerView = UIImageView(image: icon)
        marker.iconView = markerView
        marker.tracksViewChanges = true
        marker.map = googleMapsView
    }
}

extension MapViewController: GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterIconGenerator, GMUClusterRendererDelegate {
    func icon(forSize size: UInt) -> UIImage! {
        return UIImage(named: "MapPoint")
    }
    
    func setupClusterManager() {
        // Register self to listen to both GMUClusterManagerDelegate and GMSMapViewDelegate events.
        // Set up the cluster manager with the supplied icon generator and renderer.
        let clusterIconImage = UIImage(named: "MapPoint")
        let imagesArray = [clusterIconImage, clusterIconImage, clusterIconImage, clusterIconImage]
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [5, 10, 20, 100], backgroundImages: imagesArray as! [UIImage])
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        // Generate and add random items to the cluster manager.
        // Call cluster() after items have been added to perform the clustering and rendering on map.
        guard let mapView = googleMapsView else { return }
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        clusterManager?.cluster()
    }
    
    func setupClusters() {
        setupClusterManager()
        clusterManager?.setDelegate(self, mapDelegate: self)
    }
    
    func createClusters(location: CLLocationCoordinate2D) {
        let marker = GMSMarker(position: location)
        let item = POIItem(position: location, marker: marker)
        clusterManager?.add(item)
        clusterManager?.cluster()
    }
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if marker.userData as? POIItem != nil {
            let icon = UIImage(named: "MapPoint")
            marker.iconView = UIImageView(image: icon)
        }
    }
}
